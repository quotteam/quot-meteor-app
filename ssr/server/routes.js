import { Quot } from '/imports/api/quot/quot.js';
// import { QuotSource } from '/imports/api/quot/quot-source.js';

Picker.route('/watch/:id', function (params, req, res) {
  const quot = Quot.findOne({_id: params.id});
  let html = SSR.render('layout', { template: 'not_found', data: {} });
  if (quot) {
    html = SSR.render('layout', { template: 'quot', data: { quot: quot }});
  }
  res.end(html);
});
