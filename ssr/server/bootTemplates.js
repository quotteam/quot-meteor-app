SSR.compileTemplate('layout', Assets.getText('layout.html'));

Template.layout.helpers({
  getDocType: function() {
    return '<!DOCTYPE html>';
  }
});

SSR.compileTemplate('quot', Assets.getText('quot.html'));

Template.quot.helpers({
  'ogTitle': function () {
    return this.quot.title ?
           `${this.quot.title} - QUOT` : `Цитата из видео ${this.quot.description} - QUOT`;
  },
  'ogDescription': function () {
    return `Цитата из видео ${this.quot.description}`;
  },
  'ogImage': function () {
    return this.quot.thumbnail ?
      'http://ovz1.lulldev.1l0r1.vps.myjino.ru/thumb/' + this.quot.thumbnail : '/images/unknow-quot.png';
  },
  'clientRedirectURL': function () {
    return `/quot-redirect/${this.quot._id}`;
  }
});

SSR.compileTemplate('not_found', Assets.getText('not_found.html'));
Template.not_found.helpers({});
