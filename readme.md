Quot Meteor application
=======================

Installation
------------
1. Install meteor.js from official site https://www.meteor.com/
2. Clone repo with application
3. In terminal call `meteor npm install`

Configuration in settings.json
------------------------------
Development mode:
~~~
{
  "environment": "development",
  ...
~~~
Under dev mode:
- using fixtures

Testing mode:
~~~
{
  "environment": "testing",
  ...
~~~

Starting application
------------------------------
```
meteor npm start
```

