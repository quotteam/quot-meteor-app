import { TemplateController } from 'meteor/space:template-controller';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { $ } from 'meteor/jquery';
import { Session } from 'meteor/session';
import { SetTitle } from "../../helpers/title";

import './quot-search.html';

import '../quot-list/quot-list.html';


TemplateController('QuotSearch', {
  props: new SimpleSchema({
    listType: {
      type: String,
      allowedValues: ['hot', 'user']
    }
  }),
  state: {
    searchText: null,
    listType: 'hot'
  },
  private: {
    isAllowSearch (searchText) {
      return (searchText && typeof searchText === 'string' && searchText.length >= 2)
    },
    startSearch (e) {
      const searchText = $('.quot-search-field').val();
      if (this.isAllowSearch(searchText)) {
        this.state.searchText = searchText;
        Session.set('searchText', this.state.searchText);
        const searchTitle = this.state.listType === 'hot' ?
                            `${this.state.searchText} - QUOT` : `${this.state.searchText} - моя подборка`;
        SetTitle(searchTitle);
      }
      else {
        Session.set('searchText', null);
      }
    }
  },
  onCreated () {
    this.autorun(() => {
      this.state.searchText = null;
      this.state.listType = this.props.listType;
    });
  },
  onRendered () {
    Session.set('searchText', null);
  },
  helpers: {},
  events: {
    'click .search-button' (e) {
      this.startSearch(e);
    },
    'keypress .quot-search-field' (e) {
      if (e.key === 'Enter') {
        e.preventDefault();
        this.startSearch(e);
      }
    }
  }
});
