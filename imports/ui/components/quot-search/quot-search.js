import { TemplateController } from 'meteor/space:template-controller';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';

import './quot-search.html';
import '../quot-list/quot-list.html';


TemplateController('QuotSearchForm', {
  state: {
    inputIsEmpty: true,
  },
  private: {
    isAllowSearch(searchText) {
      return (searchText && typeof searchText === 'string' && searchText.length >= 2)
    },
    startSearch (e) {
      const searchText = e.target.value;
      if (this.isAllowSearch(searchText)) {
        FlowRouter.go(`/search/${searchText}`);
      }
    },
    inputIsEmpty (e) {
      const value = e.target.value;
      return value.length <= 0;
    }
  },
  onRendered () {
    this.state.inputIsEmpty = this.inputIsEmpty({
      target: $(this.firstNode).find('input')[0],
    });
  },
  helpers: {
    currentSearchRequest () {
      return FlowRouter.getParam('request');
    }
  },
  events: {
    'click .search' (e) {
      this.startSearch(e);
    },
    'click .reset' (e) {
      e.preventDefault();
      $(this.firstNode).find('input').val('');
      this.state.inputIsEmpty = this.inputIsEmpty(e);
      if (FlowRouter.current().route.name === 'search') {
        FlowRouter.go('/');
      }
    },
    'keypress .quot-search-field' (e) {
      if (e.key === 'Enter') {
        this.startSearch(e);
      }
    },
    'input .quot-search-field' (e) {
      this.state.inputIsEmpty = this.inputIsEmpty(e);
    }
  }
});
