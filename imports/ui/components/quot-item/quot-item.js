import { Meteor } from 'meteor/meteor';
import { TemplateController } from 'meteor/space:template-controller';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { moment } from 'meteor/momentjs:moment';
import './quot-item.html';
import '../quot-add/quot-add.js';


TemplateController('QuotItem', {
  props: new SimpleSchema({
    quotId: {
      type: String
    },
    from: {
      type: Number,
    },
    till: {
      type: Number,
    },
    watches: {
      type: Number,
    },
    title: {
      type: String,
      optional: true
    },
    description: {
      type: String,
      optional: true
    },
    thumbnail: {
      type: String,
      optional: true
    },
    createdAt: {
      type: Date
    },
    sourceId: {
      type: String,
      optional: true
    },
  }),
  private: {
    getThumnailLink(thumbnail) {
      return `${Meteor.settings.public.api.store.host}/thumb/${thumbnail}`;
    },
  },
  helpers: {
    thumbnailSrc: function () {
      return this.getThumnailLink(this.props.thumbnail);
    },
    formatCreatedAt: (createdAtTimestamp) =>  {
      return moment(createdAtTimestamp).fromNow();
    },
    displayQuotTitle: function () {
      const titleFromQuot = this.props.title ? this.props.title : this.props.description
      return (titleFromQuot) ? titleFromQuot : 'Без имени';
    },
    getDuration: (props) => {
      return moment.duration(props.till - props.from, 's').format('h:mm:ss', {
        trim: 'both',
        stopTrim: 'ss',
      });
    }
  }
});
