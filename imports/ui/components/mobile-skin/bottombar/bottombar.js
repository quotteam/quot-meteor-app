import { TemplateController } from 'meteor/space:template-controller';
import { $ } from 'meteor/jquery';
import './bottombar.html';
import { isActiveRoute  } from 'meteor/zimme:active-route';

import '../../login-button/login-button.js';
import '../../quot-search/quot-search.js';

TemplateController('Bottombar', {
  state: {
    isSearchExpanded : false,
    prevScrollpos: 0,
  },
  events: {
    'click .search-collapsed'() {
      this.state.isSearchExpanded = true;
    },
    'click .back'() {
      this.state.isSearchExpanded = false;
    },
  },
  onRendered() {
    $(window).scroll(() => {
      const isBiggerThanWindow = $('.container').first().height() > $(window).height();
      const currentScrollPos = window.pageYOffset;
      if (this.state.prevScrollpos - currentScrollPos > 0 || !isBiggerThanWindow) {
        $('.bottombar')[0].style.bottom = '0';
      } else {
        $('.bottombar')[0].style.bottom = '-50px';
      }
      this.state.prevScrollpos = currentScrollPos;
    });
  },
  helpers: {
  },
});