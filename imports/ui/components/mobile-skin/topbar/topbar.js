import { TemplateController } from 'meteor/space:template-controller';
import { $ } from 'meteor/jquery';
import './topbar.html';
import { isActiveRoute  } from 'meteor/zimme:active-route';

import '../../login-button/login-button.js';
import '../../quot-search/quot-search.js';

TemplateController('Topbar', {
  state: {
    prevScrollpos: 0,
  },
  events: {
  },
  onRendered() {
    $('.quot-search-field')[0].focus();
    $(window).scroll(() => {
      const isBiggerThanWindow = $('.container').first().height() > $(window).height();
      const currentScrollPos = window.pageYOffset;
      if (this.state.prevScrollpos - currentScrollPos > 0 || !isBiggerThanWindow) {
        $('.topbar')[0].style.top = '0';
      } else {
        $('.topbar')[0].style.top = '-50px';
      }
      this.state.prevScrollpos = currentScrollPos;
    });
  },
  helpers: {
  },
});