import { TemplateController } from 'meteor/space:template-controller';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';

import './player.html';

import { Quot } from '/imports/api/quot/quot.js';
import { IncrementView } from '/imports/api/quot/methods.js';
import { SetTitle } from '../../helpers/title';

import '../preloader/preloader.js';
import '../quot-add/quot-add.js';
import '../social-share/modal/modal.js';
import '../html-player/htmlPlayer.js';

const DEFAULT_VIDEO_QUALITY = 'lowest';

TemplateController('QuotPlayer', {
  state: {
    isLoaded: false,
    isPlaying: false,
    lists: [],
    isPlayerReady: false,
    ytPlayer: null,
    srcInitialized: false,
    src: '',
    from: 0,
    till: 0
  },
  private: {
    play() {
      if (this._htmlPlayer.currentTime >= this.state.till) {
        this._htmlPlayer.currentTime = this.state.from;
      }

      this._htmlPlayer.play();
      this.state.isPlaying = true;
    },

    pause() {
      this._htmlPlayer.pause();
      this.state.isPlaying = false;
    },
    getQuot () {
      return Quot.findOne(FlowRouter.getParam('id'));
    },
    _initPlayerEvents() {
      const pauseAtIntervalEnd = () => {
        if (this._htmlPlayer.currentTime >= this.state.till) {
          this._htmlPlayer.pause();
          this._htmlPlayer.currentTime = this.state.from;
        }
      };
      this._htmlPlayer.addEventListener('pause', () => {
        this.state.isPlaying = false;
      });
      this._htmlPlayer.addEventListener('play', () => {
        this.state.isPlaying = true;
      });
      this._htmlPlayer.addEventListener('timeupdate', pauseAtIntervalEnd);
    },
    _initPlayer () {
    },
    getQuotTitle () {
      const title = this.getQuot().title;
      const description = this.getQuot().description ? this.getQuot().description : '';
      return title ? title : 'Момент из видео: ' + description;
    },
    getThumnailLink(thumbnail) {
      return `${Meteor.settings.public.api.store.host}/thumb/${thumbnail}`;
    },
  },
  onRendered() {
    if ($('#modal-social-share').length === 0) {
      Blaze.render(Template.ModalSocialShare, $('body')[0]);
    }
  },
  onCreated () {
    this.autorun(() => {
      Blaze.Template.htmlPlayer.onRendered(() => {
        this._htmlPlayer = $('#html-player')[0];
        this._initPlayerEvents();
        this.state.isPlayerReady = true;
      });
      this.subscribe('Quot.getOne', FlowRouter.getParam('id'), {
        onReady: () => {
          if (!this.getQuot()) {
            FlowRouter.redirect('/404');
            return;
          }
          SetTitle(this.getQuotTitle());
          this.state.from = this.getQuot().interval.from;
          this.state.till = this.getQuot().interval.till;
          IncrementView.call({ quotId: this.getQuot()._id });
          this._initPlayer();
          Meteor.call('Youtube.getVideoSrc', this.getQuot().video_id, DEFAULT_VIDEO_QUALITY, (err, srcObj) => {
            if (err) {
              ErrorHandler.register(ErrorHandler.SYSTEM_CLIENT_ERROR,
                { loggerMessage: 'Youtube.isVideoExist in creator err', err: err, videoId: this.state.youtubeVideoId },
                () => Notification.errorMessage(ErrorHandler.CLIENT_ERROR_NOTIFICATION_MSG)
              );
              console.log(err.message);
              return;
            }
    
            if (srcObj) {
              this.state.src = srcObj.url + "#t=" + this.getQuot().interval.from;
              this.state.srcInitialized = true;
            }
            else {
              console.log('NIHUA');
            }
          });
        }
      });
    });
  },
  helpers: {
    targetQuot () {
      return this.getQuot();
    },
    isPlayerReady () {
      return this.state.isPlayerReady;
    },
    isDisplayPlayer () {
      return !this.state.isPlayerReady ? 'd-none' : '';
    },
    thumbnailSrc () {
      return this.getThumnailLink(this.getQuot().thumbnail);
    },
    getQuotTitle () {
      return this.getQuotTitle();
    },
  },
  events: {
    'click #html-player' (e) {
      if (this.state.isPlaying) {
        this.pause();
        return;
      }

      this.play();
    },
    'click .btn-share' (e) {
      $('#modal-social-share').modal('show');
    },
  }
});
