import {Blaze} from 'meteor/blaze';
import {Meteor} from 'meteor/meteor';
import {TemplateController} from 'meteor/space:template-controller';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {$} from 'meteor/jquery';

import './login-button.html';
import '../social-login/modal/modal.html';
import '../social-login/social-login';


TemplateController('LoginButton', {
  onRendered() {
    if ($('#modal-social-login').length === 0) {
      Blaze.render(Template.ModalSocialLogin, $('body')[0]);
    }
  },
  private: {
    isCollectionPage() {
      return FlowRouter.getRouteName() === 'my-collection';
    },
    getUserProfileImage() {
      const userProfile = Meteor.user() ? Meteor.user().profile : null;
      return (userProfile && userProfile.image) ? userProfile.image : null;
    }
  },
  helpers: {
    getIcon() {
      if (Meteor.userId()) {
        const userImage = this.getUserProfileImage();
        return userImage ? userImage : 'images/user.svg';
      }
      return 'images/login.svg';
    },
    getStatus() {
      return this.getUserProfileImage() ? 'user-image' : 'without_image';
    },
  },
  events: {
    'click .login-button'() {
      if (!Meteor.userId()) {
        $('#modal-social-login').modal('show');
      }
    },
    'click .exit-item'() {
      Meteor.logout();
    }
  },
});
