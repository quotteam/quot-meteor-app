import { $ } from 'meteor/jquery';
import { TemplateController } from 'meteor/space:template-controller';
import { YT } from 'meteor/adrianliaw:youtube-iframe-api';
import { Session } from 'meteor/session';
import { moment } from 'meteor/momentjs:moment';
import { DurationFormat } from 'meteor/oaf:moment-duration-format';
import { DistanceSpinnersController } from '/imports/api/cutter/client/spinner-controller.js';
import { Meteor } from 'meteor/meteor';
import { Blaze } from 'meteor/blaze';
import { ErrorHandler } from "../../../api/error-handler/error-handler";
import { Notification } from '/imports/api/notification/notification.js';

import './cutter.html';
import '../cutter/cutter.js';
import '../html-player/htmlPlayer.js';

const DEFAULT_VIDEO_QUALITY = 'lowest';

TemplateController('Cutter', {

  state: {
    yotubeVideoId: null,
    timelineInterval: {
      from: 0,
      till: 0
    },
    srcInitialized: false,
    src: '',
    isPlaying: false,
  },

  helpers: {
    isDisplayReady () {
      return !this.state.srcInitialized ? 'd-none' : '';
    }
  },

  onCreated () {
    this.autorun(() => {
      this.state.youtubeVideoId = Session.get('youtubeVideoId');
    });

    Blaze.Template.htmlPlayer.onCreated(() => {
      Session.set('isCutterPlayerLoaded', this.state.srcInitialized);
    });

    Blaze.Template.htmlPlayer.onRendered(() => {
      this._htmlPlayer = $('#html-player')[0];

      this._htmlPlayer.addEventListener('loadedmetadata', () => {
        this._initCutter();
      });
    });

    this._initPlayer();
  },

  events: {
    'mousedown .left-spinner .ear' (e) {
      this._spinnerController._onLeftSpinnerPress(e);
    },
    'mousedown .right-spinner .ear' (e) {
      this._spinnerController._onRightSpinnerPress(e);
    },
    'touchstart .left-spinner .ear' (e) {
      this._spinnerController._onLeftSpinnerPress(e);
    },
    'touchstart .right-spinner .ear' (e) {
      this._spinnerController._onRightSpinnerPress(e);
    },
    'mouseover .spinner .ear' (e) {
      e.target.classList.add('hover');
    },
    'mouseleave .spinner .ear' (e) {
      e.target.classList.remove('hover');
    },

    'click .play-button' () {
      if (this.state.isPlaying) {
        this.pause();
        return;
      }

      this.play();
    },
  },

  private: {

    leftSpinner () {
      return $('.left-spinner')[0];
    },

    rightSpinner () {
      return $('.right-spinner')[0];
    },

    slider () {
      return $('.slider')[0];
    },

    timelineContainer () {
      return $('.timeline')[0];
    },

    play() {
      const slider = this._spinnerController.slider();
      if (slider.getPosition() >= this._spinnerController.rightSpinner().getPosition()) {
        this._htmlPlayer.currentTime = this.state.from;
        slider.setPosition(this._spinnerController.leftSpinner().getPosition());
      }
      else if (this._convertSpinnerPosToTime(this._spinnerController.rightSpinner()) == this._htmlPlayer.currentTime) {
        this._htmlPlayer.currentTime = this._convertSpinnerPosToTime(slider);
      }

      this._htmlPlayer.play();
      this.state.isPlaying = true;
    },

    pause() {
      this._htmlPlayer.pause();
      this.state.isPlaying = false;
    },

    _initPlayer () {
      Meteor.call('Youtube.getVideoSrc', this.state.youtubeVideoId, DEFAULT_VIDEO_QUALITY, (err, srcObj) => {
        if (err) {
          ErrorHandler.register(ErrorHandler.SYSTEM_CLIENT_ERROR,
            { loggerMessage: 'Youtube.isVideoExist in creator err', err: err, videoId: this.state.youtubeVideoId },
            () => Notification.errorMessage(ErrorHandler.CLIENT_ERROR_NOTIFICATION_MSG)
          );
          console.log(err.message);
          return;
        }

        if (srcObj) {
          this.state.src = srcObj.url;
          this.state.srcInitialized = true;
        }
        else {
          console.log('NIHUA');
        }
      });

    },

    _initCutter () {
      this._spinnerController =
        new DistanceSpinnersController(this.timelineContainer(), this.leftSpinner(), this.rightSpinner(), this.slider());
      this._initSpinnersEvents();
      this._initPlayerEvents();
      this._spinnerController.rightSpinner(this._htmlPlayer.duration);
      this._spinnerController.rightSpinner().setPosition(100);
      this._spinnerController.leftSpinner().setPosition(0);
    },

    /**
     * @private
     */
    _initSpinnersEvents() {
      const leftSpinner = this._spinnerController.leftSpinner();
      const rightSpinner = this._spinnerController.rightSpinner();

      leftSpinner.positionChangedEvent().register(() => {
        this._spinnerController.slider().setPosition(leftSpinner.getPosition());
        this.pause();
        this._updatePositionView(leftSpinner);
        this.state.from = Number(this._convertSpinnerPosToTime(leftSpinner));
        Session.set('intervalFrom', this.state.from);
      });
      rightSpinner.positionChangedEvent().register(() => {
        this._updatePositionView(rightSpinner);
        this.state.till = Number(this._convertSpinnerPosToTime(rightSpinner));
        Session.set('intervalTill', this.state.till);
      });
    },

    /**
     * @param {!Spinner} spinner
     * @private
     */
    _updatePositionView (spinner) {
      this._htmlPlayer.currentTime = Number(this._convertSpinnerPosToTime(spinner));
      const newTimePosition = this._htmlPlayer.currentTime;
      const timeFormat = (newTimePosition <= 3600) ? 'mm:ss' : 'h:mm:ss';
      spinner.setLabel(moment.utc(newTimePosition * 1000).format(timeFormat));
    },

    _initPlayerEvents() {
      const slider = this._spinnerController.slider();

      const startSliderRunning = () => {
        const positionInPercent = this._spinnerController.rightSpinner().getPosition();
        slider.runTo(positionInPercent, (this.state.till - this._htmlPlayer.currentTime) * 1000);
        this.state.isPlaying = true;
      };
      this._htmlPlayer.addEventListener('play', startSliderRunning);

      const stopSliderRunning = () => {
        slider.stop();
        this.state.isPlaying = false;
      };
      this._htmlPlayer.addEventListener('pause', stopSliderRunning);

      const pauseAtIntervalEnd = () => {
        if (this._htmlPlayer.currentTime >= this.state.till) {
          this._htmlPlayer.pause();
        }
      };
      this._htmlPlayer.addEventListener('timeupdate', pauseAtIntervalEnd);
    },

    /**
     * @param {!Spinner} spinner
     * @return {number}
     * @private
     */
    _convertSpinnerPosToTime (spinner) {
      const result = this._htmlPlayer.duration / 100 * spinner.getPosition();
      return Number((result).toFixed(6));
    },
  }
});
