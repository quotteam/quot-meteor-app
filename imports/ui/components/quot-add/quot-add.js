import { TemplateController } from 'meteor/space:template-controller';
import { Meteor } from 'meteor/meteor';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Blaze } from 'meteor/blaze';
import { $ } from 'meteor/jquery';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { StateEvent } from "../../../api/state-event/state-event";
import { ErrorHandler } from "../../../api/error-handler/error-handler";
import { Notification } from '/imports/api/notification/notification.js';

import './quot-add.html';

import '../social-login/modal/modal.html';


TemplateController('QuotAdd', {
  props: new SimpleSchema({
    quotId: {
      type: String
    },
    cardMode: {
      type: Boolean,
      optional: true
    },
  }),
  state: {
    /*
     * pending
     * added
     * add
     * */
    status: 'pending',
    isShowLoginModal: false
  },
  private: {},
  onCreated () {
    this.autorun(() => {
      Meteor.call('Quots.UserCollection.isExistQuot', { quotId: this.props.quotId }, (err, res) => {
        if (err) {
          ErrorHandler.register(ErrorHandler.SYSTEM_CLIENT_ERROR,
            { loggerMessage: 'Quot not found for add button', err: err, quotId: this.props.quotId },
            () => Notification.errorMessage(ErrorHandler.CLIENT_ERROR_NOTIFICATION_MSG)
          );
          return;
        }
        this.state.status = res ? 'added' : 'add';
      });
    });
    if ($('#modal-social-login').length === 0) {
      Blaze.render(Template.ModalSocialLogin, $('body')[0]);
    }
  },
  onRendered () {
    StateEvent.run('UserCreatedQuot', () => {
      setTimeout(() => {
        if (FlowRouter.current().route.name === 'quot' && !Meteor.userId()) {
          $('.btn-add-quot').popover({
            container: '.quot-add',
            content: `<span id="close" class="close" onclick="$('.btn-add-quot').popover('dispose');">&times;</span>
        Выполните вход в аккаунт для сохранения созданного quot в коллекцию`,
            html: true,
            placement: 'right'
          }).popover('show');
        }
      }, 3000);
    });
  },
  helpers: {
    isLoaded () {
      return this.state.status !== 'pending';
    },
    isAdded () {
      return this.state.status === 'added';
    },
    btnDisplayStatus () {
      let statusClasses = 'disabled';
      switch (this.state.status) {
        case 'added':
          statusClasses = this.props.cardMode ? 'success' : 'btn-outline-danger';
          break;
        case 'add':
          statusClasses = this.props.cardMode ? 'outline-success' : 'btn-outline-success';
          break;
      }
      return statusClasses;
    }
  },
  events: {
    'click .btn-add-quot' (e) {
      e.preventDefault();
      if (!Meteor.userId()) {
        $('#modal-social-login').modal('show');
        return;
      }
      const isAdded = e.currentTarget.classList.contains('btn-success') || e.currentTarget.classList.contains('success');
      const actionMethod = (isAdded) ? 'Quots.UserCollection.remove' : 'Quots.UserCollection.add';
      Meteor.call(actionMethod, { quotId: this.props.quotId }, (err, res) => {
        if (err) {
          ErrorHandler.register(ErrorHandler.SYSTEM_CLIENT_ERROR,
            { loggerMessage: 'Error in add button', err: err, quotId: this.props.quotId, actionMethod: actionMethod },
            () => Notification.errorMessage(ErrorHandler.CLIENT_ERROR_NOTIFICATION_MSG)
          );
          return;
        }
      });
      this.state.status = (isAdded) ? 'add' : 'added';
    }
  }
});
