import { TemplateController } from 'meteor/space:template-controller';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import './htmlPlayer.html';

TemplateController('htmlPlayer', {
  props: new SimpleSchema({
    src: {
      type: String,
    }
  }),
  state: {
    src: '',
  },
  private: {
    onCreated() {},
    helpers: {},
    events: {}
  }
});
