import { TemplateController } from 'meteor/space:template-controller';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Session } from 'meteor/session';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { InfiniteScroll } from '/imports/api/infinite-scroll/client/infinite-scroll.js';
import { SetTitle } from '../../helpers/title';

import '../preloader/preloader.js';
import './quot-list.html';
import '../quot-item/quot-item.js';

import { Quot } from '/imports/api/quot/quot.js';


TemplateController('QuotList', {
  props: new SimpleSchema({
    listType: {
      type: String,
      allowedValues: ['hot', 'user']
    }
  }),
  state: {
    isListLoaded: false
  },
  private: {
    infiniteLimit: 'quotListLimit'
  },
  onCreated () {
    this.state.infiniteScroll = new InfiniteScroll('#showMoreResults', this.infiniteLimit, 10);
    this.state.isListLoaded = false;
    this.autorun(() => {
      const requestParam = FlowRouter.getParam('request');
      let searchParam = '';

      if (requestParam) {
        searchParam = requestParam;
        SetTitle(`${searchParam} - QUOT`);
      }

      this.subscribe(`QuotCollection.${this.props.listType}.search`,
        searchParam, Session.get(this.infiniteLimit), {
          onReady: () => {
            this.state.isListLoaded = true;
          }
        });
    });
  },
  helpers: {
    quotsList: function () {
      if (this.props.listType === 'user') {
        return Quot.find({}, { sort: { addedAt: -1 } });
      }
      return Quot.find({});
    },
    moreResults: () => {
      return !(Quot.find().count() < Session.get(this.infiniteLimit));
    },
    isExistQuots: () => {
      return (Quot.find().count() > 0);
    }
  }
});

