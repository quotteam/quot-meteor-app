import { Meteor } from 'meteor/meteor';
import { $ } from 'meteor/jquery';
import { TemplateController } from 'meteor/space:template-controller';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ErrorHandler } from '/imports/api/error-handler/error-handler.js';
import { Notification } from '/imports/api/notification/notification.js';

import './social-login.html';

TemplateController('SocialLogin', {
  props: new SimpleSchema({
    btnSize: {
      type: String,
      allowedValues: ['xs', 'sm', 'md', 'lg'],
      optional: true
    }
  }),
  private: {
    handleError (err) {
      if (err) {
        ErrorHandler.register(ErrorHandler.SYSTEM_ERROR,
          { loggerMessage: 'Social login error', err: err },
          () => Notification.errorMessage(ErrorHandler.CLIENT_ERROR_NOTIFICATION_MSG)
        );
      }
    },
    authUser (social) {
      if (social === 'vk') {
        Meteor.loginWithVk((err) => this.handleError(err));
      } else if (social === 'facebook') {
        Meteor.loginWithFacebook((err) => this.handleError(err));
      } else if (social === 'google') {
        Meteor.loginWithGoogle((err) => this.handleError(err));
      } else if (social === 'twitter') {
        Meteor.loginWithTwitter((err) => this.handleError(err));
      }
    }
  },
  helpers: {
    btnSizeClass () {
      return `btn-${this.props.btnSize}`;
    }
  },
  events: {
    'click .social-button': function (e) {
      const social = $(e.target).data('social');
      this.authUser(social);
    }
  }
});
