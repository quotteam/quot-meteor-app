import { TemplateController } from 'meteor/space:template-controller';
import { $ } from 'meteor/jquery';
import './navbar.html';
import { isActiveRoute  } from 'meteor/zimme:active-route';

import '../../components/login-button/login-button.js';
import '../../components/quot-search/quot-search.js';

TemplateController('Navbar', {
  state: {
    isSearchExpanded : false,
  },
  events: {
    'click .search-collapsed'() {
      this.state.isSearchExpanded = true;
    },
    'click .back'() {
      this.state.isSearchExpanded = false;
    },
  },
  helpers: {
    isSearchExpanded() {
      return this.state.isSearchExpanded ? 'active' : '';
    }
  },
});