import { TemplateController } from 'meteor/space:template-controller';
import { $ } from 'meteor/jquery';
import { InsertQuot } from '../../../api/quot/methods.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import validator from 'validator';
import { ErrorHandler } from '/imports/api/error-handler/error-handler.js';
import { Notification } from '/imports/api/notification/notification.js';

import { StateEvent } from '/imports/api/state-event/state-event.js';
import { parseYoutubeVideoId } from '/imports/api/youtube/youtube.js';
import './creator.html';

import '../cutter/cutter.js';
import '../preloader/preloader.js';


TemplateController('Creator', {
  state: {
    canShowCreator: false,
    yotubeVideoLink: null,
    isCutterPlayerReady: false,
    isLoadingInProgress: false,
    isURLInvalid: false,
    isVideoInvalid: false
  },
  helpers: {
    isCutterReady () {
      return this.state.isCutterPlayerReady;
    },
    isDisplayCreator () {
      return !this.state.isCutterPlayerReady ? 'd-none' : '';
    }
  },
  events: {
    'input .target-link' (e) {
      this.state.isLoadingInProgress = true;
      const inputValue = $(e.target).val().trim();

      if (inputValue === this.state.yotubeVideoLink) {
        this.state.isLoadingInProgress = false;
        return;
      }
      else if (inputValue.length === 0) {
        this.state.isLoadingInProgress = false;
        this.state.isURLInvalid = false;
        return;
      }

      const videoId = parseYoutubeVideoId(inputValue);
      if (validator.isURL(inputValue) && videoId) {
        this.prepareYoutubeVideo(videoId, inputValue);
      }
      else {
        this.state.isVideoInvalid = false;
        this.state.isURLInvalid = true;
        this.state.isLoadingInProgress = false;
        this.state.canShowCreator = false;
        this.state.yotubeVideoLink = null;
      }
    },
    'keyup .title-field' (e) {
      const title = $(e.target).val();
      if (this.state.title !== title) {
        this.state.title = title;
        Session.set('title', this.state.title);
      }
    },
    'click .complete-button' () {
      if (!Session.get('youtubeVideoId')) {
        return;
      }
      const fromValue = Number(Session.get('intervalFrom').toFixed(0));
      const tillValue = Number(Session.get('intervalTill').toFixed(0));
      InsertQuot.call({
        video_id: Session.get('youtubeVideoId'),
        interval: {
          from: fromValue,
          till: tillValue,
        },
        title: Session.get('title'),
        description: Session.get('description')
      }, (err, res) => {
        if (err) {
          ErrorHandler.register(ErrorHandler.SYSTEM_ERROR,
            { loggerMessage: 'Insert quot error in creator', err: err, videoId: video_id },
            () => Notification.errorMessage(ErrorHandler.CLIENT_ERROR_NOTIFICATION_MSG)
          );
          return;
        }
        const quotId = res;

        if (Meteor.userId()) {
          this.addQuotToUser(quotId);
        }
        else {
          Hooks.init();
          Hooks.onLoggedIn = () => {
            this.addQuotToUser(quotId);
            Hooks.onLoggedIn = null;
          };
        }

        Session.set('youtubeVideoId', null);
        Session.set('title', null);
        Session.set('description', null);

        StateEvent.register('UserCreatedQuot', 'persistent');

        FlowRouter.go(`/watch/${quotId}`);
      });
    },
  },
  private: {
    prepareYoutubeVideo (youtubeVideoId, youtubeLink) {
      this.state.canShowCreator = false;
      this.state.yotubeVideoLink = null;
      Meteor.call('Youtube.isAllowEmbedVideo', youtubeVideoId, (err, isAllowEmbedVideo) => {
        if (err) {
          ErrorHandler.register(ErrorHandler.SYSTEM_CLIENT_ERROR,
            { loggerMessage: 'Youtube.isVideoExist in creator err', err: err, videoId: youtubeVideoId },
            () => Notification.errorMessage(ErrorHandler.CLIENT_ERROR_NOTIFICATION_MSG)
          );
          // TODO: handle server error
          this.state.isVideoInvalid = true;
          this.state.isLoadingInProgress = false;
          this.state.canShowCreator = false;
          return;
        }
        this.state.isVideoInvalid = false;
        this.state.isURLInvalid = false;
        this.state.canShowCreator = true;
        this.state.yotubeVideoLink = youtubeLink;
        Session.set('youtubeVideoId', youtubeVideoId);
        setTimeout(() => {
          this.state.isLoadingInProgress = !this.state.isCutterPlayerReady;
        }, 3000);
      });
    },
    addQuotToUser (quotId) {
      Meteor.call('Quots.UserCollection.add', {
        quotId: quotId,
      }, (err) => {
        if (err) {
          ErrorHandler.register(
            ErrorHandler.SYSTEM_CLIENT_ERROR,
            { loggerMessage: 'Error add quot after creator', err: err, quotId: quotId },
            () => Notification.errorMessage(ErrorHandler.CLIENT_ERROR_NOTIFICATION_MSG)
          );
        }
      });
    },
  },
  onCreated () {
    Session.set('isCutterPlayerLoaded', false);
    this.autorun(() => {
      this.state.isCutterPlayerReady = Session.get('isCutterPlayerLoaded');
    });
  }
});
