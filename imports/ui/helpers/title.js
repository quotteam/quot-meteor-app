import { Session } from 'meteor/session';


/**
 * Set page title
 *
 * @param {string} title
 * @constructor
 */
export const SetTitle = (title) => {
  document.title = title;
};
