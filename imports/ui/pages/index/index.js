import { TemplateController } from 'meteor/space:template-controller';
import { SetTitle } from '../../helpers/title';
import '../../components/navbar/navbar.js';
import '../../components/quot-list/quot-list.js';
import './index.html';


TemplateController('IndexPage', {
  onCreated () {
    SetTitle('QUOT - максимально быстрое цитирование роликов');
  }
});
