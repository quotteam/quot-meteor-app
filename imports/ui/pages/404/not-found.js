import { TemplateController } from 'meteor/space:template-controller';
import { SetTitle } from '../../helpers/title';
import './not-found.html';



TemplateController('NotFoundPage', {
  onCreated () {
    SetTitle('404 Страница не найдена');
  }
});
