import { TemplateController } from 'meteor/space:template-controller';

import '../../components/quot-list/quot-list.js';
import '../../components/social-login/social-login.js';

import './user-collection.html';

import { SetTitle } from "../../helpers/title";

TemplateController('QuotUserCollection', {
  onCreated() {
    this.autorun(() => {
      this.subscribe('User.data', {
        onReady: () => {
          let collectionTitle = `Моя коллекция Quot'ов`;
          if (Meteor.userId() && Meteor.user().profile.name) {
            collectionTitle = `${Meteor.user().profile.name} - коллекция Quot'ов`;
          }
          SetTitle(collectionTitle);
        }
      });
    });
  },
});
