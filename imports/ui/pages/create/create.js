import { TemplateController } from 'meteor/space:template-controller';
import { SetTitle } from '../../helpers/title';
import './create.html';
import '../../components/creator/creator.js';


TemplateController('CreatorPage', {
  onCreated () {
    SetTitle('Создать новый Quot');
  }
});
