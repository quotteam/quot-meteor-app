import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { Reset } from './reset.js';

// Import Layouts
import '/imports/ui/layouts';

// Import Pages
import '/imports/ui/pages/index/index.js';
import '/imports/ui/pages/user-collection/user-collection.js';
import '/imports/ui/pages/quot-player/quot-player.js';
import '/imports/ui/pages/create/create.js';
import '/imports/ui/pages/404/not-found.js';

// Home route
FlowRouter.route('/', {
  name: 'index',
  triggersEnter: [Reset],
  action() {
    BlazeLayout.reset();
    BlazeLayout.render('PublicLayout', { main: 'IndexPage'});
  },
});

FlowRouter.route('/search', {
  name: 'empty-search',
  action () {
    FlowRouter.go('/search/ ');
  },
});

FlowRouter.route('/search/:request', {
  name: 'search',
  triggersEnter: [Reset],
  action () {
    BlazeLayout.reset();
    BlazeLayout.render('PublicLayout', { main: 'IndexPage' });
  },
});

FlowRouter.route('/watch/:id', {
  name: 'quot',
  triggersEnter: [Reset],
  action() {
    BlazeLayout.reset();
    BlazeLayout.render('PublicLayout', { main: 'QuotPlayerPage' }, { force: true });
  }
});

FlowRouter.route('/quot-redirect/:id', {
  triggersEnter: [function(context, redirect) {
    redirect(`/watch/${context.params.id}`);
  }],
});

FlowRouter.route('/create', {
  name: 'create',
  triggersEnter: [Reset],
  action() {
    BlazeLayout.reset();
    BlazeLayout.render('PublicLayout', { main: 'CreatorPage' });
  }
});

FlowRouter.route('/my-collection', {
  name: 'my-collection',
  triggersEnter: [Reset],
  action() {
    BlazeLayout.reset();
    BlazeLayout.render('PublicLayout', { main: 'QuotUserCollection' });
  },
});

FlowRouter.route('/404', {
  name: 'not-found',
  triggersEnter: [Reset],
  action() {
    BlazeLayout.render('PublicLayout', { main: 'NotFoundPage' });
  }
});


FlowRouter.route('/watch/', {
  name: 'not-found',
  triggersEnter: [Reset],
  action() {
    BlazeLayout.render('PublicLayout', { main: 'NotFoundPage' });
  }
});
