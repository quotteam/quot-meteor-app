import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Tracker } from 'meteor/tracker';
import { $ } from 'meteor/jquery';
import { FlowRouter } from 'meteor/kadira:flow-router';

Tracker.autorun(function () {
  if (Meteor.userId()) {
    $('#modal-social-login').modal('hide');
  }
});

Accounts.onLogin(() => {
  FlowRouter.go('/my-collection');
});

Accounts.onLogout(() => {
  if (FlowRouter.current().path === '/') {
    location.reload();
  }
  FlowRouter.go('/');
});
