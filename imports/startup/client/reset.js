import { Notification } from '/imports/api/notification/notification.js';


export const Reset = () => {
  Notification.reset();
};