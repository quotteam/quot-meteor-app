import { Meteor } from 'meteor/meteor';
import { QuotFixture } from '../../api/quot/server/fixtures.js';
import { isDevelopment } from '../../utils/environment.js';

Meteor.startup(() => {
  if (isDevelopment()) {
    QuotFixture();
  }
});
