import { Accounts } from 'meteor/accounts-base';

Accounts.onCreateUser(function (options, user) {
  user.profile = {};
  if (user.services.hasOwnProperty('twitter')) {
    user.profile.name = user.services.twitter.screenName;
    user.profile.image = user.services.twitter.profile_image_url;
  }
  else if (user.services.hasOwnProperty('google')) {
    user.profile.name = user.services.google.name;
    user.profile.image = user.services.google.picture;
  }
  else if (user.services.hasOwnProperty('vk')) {
    user.profile.name = `${user.services.vk.last_name} ${user.services.vk.first_name}`;
    user.profile.image = user.services.vk.photo;
  }
  else if (user.services.hasOwnProperty('facebook')) {
    user.profile.name = `${user.services.facebook.name}`;
    // user.profile.image = user.services.vk.photo;
  }
  return user;
});
