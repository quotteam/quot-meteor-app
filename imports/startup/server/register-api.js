// Quot
import '../../api/quot/methods.js';
import '../../api/quot/server/publications.js';

// Quot collection
import '../../api/quot-collection/hot/server/publications.js';
import '../../api/quot-collection/user/server/publications.js';
import '../../api/quot-collection/user/server/methods.js';

// Youtube
import '../../api/youtube/server/methods.js';

// Thumbnail
import '../../api/thumbnail/server/methods.js';

// User
import '../../api/user/user.js';
import '../../api/user/server/publications.js';

