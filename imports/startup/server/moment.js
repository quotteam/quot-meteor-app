import { Meteor } from 'meteor/meteor';
import moment from 'moment';

Meteor.startup(function() {
  moment.locale('ru');
  moment.lang('ru');
  moment.tz.setDefault('Russia');
});