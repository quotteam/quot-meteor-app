import { Meteor } from 'meteor/meteor';
import { ServiceConfiguration } from 'meteor/service-configuration';

Meteor.startup(function () {
  const OAuthConfig = Meteor.settings.oauth;

  ServiceConfiguration.configurations.upsert({
    service: 'facebook'
  }, {
    $set: {
      appId: OAuthConfig.facebook.appId,
      secret: OAuthConfig.facebook.secret
    }
  });

  ServiceConfiguration.configurations.upsert({
    service: 'google'
  }, {
    $set: {
      clientId: OAuthConfig.google.appId,
      secret: OAuthConfig.google.secret
    }
  });

  ServiceConfiguration.configurations.upsert({
    service: 'twitter'
  }, {
    $set: {
      consumerKey: OAuthConfig.twitter.appId,
      secret: OAuthConfig.twitter.secret
    }
  });

  ServiceConfiguration.configurations.upsert({
    service: 'vk'
  }, {
    $set: {
      appId: OAuthConfig.vk.appId,
      secret: OAuthConfig.vk.secret,
      scope: OAuthConfig.vk.scope
    }
  });

  return;
});