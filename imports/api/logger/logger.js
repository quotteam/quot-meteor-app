import { Logger } from 'meteor/ostrio:logger';
import { LoggerMongo } from 'meteor/ostrio:loggermongo';

/*
 message {String} - Any text message
 data    {Object} - [optional] Any additional info as object
 userId  {String} - [optional] Current user id
 -------
 log.info(message, data, userId);
 log.debug(message, data, userId);
 log.error(message, data, userId);
 log.fatal(message, data, userId);
 log.warn(message, data, userId);
 log.trace(message, data, userId);
 */

const log = new Logger();
(new LoggerMongo(log)).enable();

//(new LoggerMongo(log)).enable({
//  client: true,
//  server: true
//});

export const logger = log;
