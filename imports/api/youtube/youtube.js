const urlParse = require('url-parse');

/**
 * Parse youtubeId from youtube url
 *
 * @param {string} youtubeUrl
 * @returns {string|null} youtube video id or null
 */
export const parseYoutubeVideoId = (youtubeUrl) => {
  const parsedParams = urlParse(youtubeUrl, true);
  let videoId = null;
  if (parsedParams.host === 'youtube.com' || parsedParams.host === 'www.youtube.com') {
    if (parsedParams.query.hasOwnProperty('v') && typeof parsedParams.query.v === 'string' &&
      parsedParams.query.v.length > 0) {
      videoId = parsedParams.query.v;
    }
  }
  else if (parsedParams.host === 'youtu.be') {
    const path = parsedParams.pathname;
    if (Object.getOwnPropertyNames(parsedParams.query).length === 0 && path.length > 1 && path[0] === '/') {
      videoId = path.substring(1);
    }
  }
  else {
    return null;
  }

  return videoId;
};
