import { Meteor } from 'meteor/meteor';
import YouTube from 'youtube-node-plus';
import x2js from 'x2js';
import { HTTP } from 'meteor/http';
import { check } from 'meteor/check';
const ytdl = require('ytdl-core');
import { logger } from '/imports/api/logger/logger.js';
import { ErrorHandler } from "../../error-handler/error-handler";

let Future = Npm.require('fibers/future');

// TODO: add check
// TODO: ебнуть либо, взяв получение субтитлов из кода
class YoutubeWrapper {

  constructor() {
    try {
      this.yotubeV3API = new YouTube(Meteor.settings.api.youtube.v3.key);
      this.allowSubtitlesLanguage = ['en', 'ru'];
    }
    catch (e) {
      ErrorHandler.register(ErrorHandler.SYSTEM_ERROR,
        { loggerMessage: 'Youtube configuration error', youtubeConfig: Meteor.settings.api.youtube.v3 });
      throw new Meteor.Error('YoutubeError', 'Youtube configuration error', e);
    }
  }

  /**
  * Get youtube video information by video id
  * @param {string} videoId
  * @return {json}
  */
  getVideoInfo(videoId) {
    check(videoId, String);
    const future = new Future();
    this.yotubeV3API.getById(videoId, (error, result) => {
      if (error) {
        future.return(error);
      }
      else {
        future.return(result);
      }
    });
    return future.wait();
  }

  /**
  * Get youtube video information by video id
  * @param {string} videoId
  * @return {json}
  */
  getCaptionListByVideoId(videoId) {
    check(videoId, String);
    const future = new Future();
    this.yotubeV3API.getCaptionListByVideoId(videoId, (error, result) => {
      if (error) {
        future.return(error);
      }
      else {
        future.return(result);
      }
    });
    return future.wait();
  }

  /**
  * Get all subtitles from Youtube API/timedtext for multiple languages
  * @param {string} videoId
  * @param {string} language - short lang
  * @return {json|null}
  */
  getVideoSubtitles(videoId) {
    check(videoId, String);
    const xml2json = new x2js();
    const future = new Future();
    let xmlSubtitlesResult = '';
    this.allowSubtitlesLanguage.forEach((lang, i) => {
      HTTP.get('https://www.youtube.com/api/timedtext', {params: {v: videoId, lang: lang}}, (error, result) => {
        if (error) {
          return;
        }
        else {
          if (result.statusCode === 200 && result.content.length > 0) {
            xmlSubtitlesResult += result.content;
          }
          if (i === (this.allowSubtitlesLanguage.length - 1)) {
            if (xmlSubtitlesResult.length > 0) {
              future.return(xml2json.xml2js(xmlSubtitlesResult));
            } else {
              future.return(null);
            }
          }
        }
      });
    });
    return future.wait();
  }

  /**
   * Get youtube video src information by video id
   * @param {string} videoId
   * @param {string} quality
   * @return {json}
   */
  getVideoSrc(videoId, quality) {
    check(videoId, String);
    check(quality, String);
    const future = new Future();
    ytdl.getInfo(videoId, (error, result) => {
      if (error) {
        future.return(error);
      }
      else {
        const format = ytdl.chooseFormat(result.formats, {
          quality: quality,
          filter: 'video'
        });
        future.return(format);
      }
    });
    return future.wait();
  }
}

export default Youtube = new YoutubeWrapper();
