import { Meteor } from 'meteor/meteor';
import Youtube from './youtube.js';
import { check } from 'meteor/check';

Meteor.methods({
  /**
  * Check exist video on Youtube by id
  * @param {string} videoId
  * @return {boolean}
  */
  'Youtube.isVideoExist'(videoId) {
    check(videoId, String);
    const videoInfo = Youtube.getVideoInfo(videoId);
    return videoInfo.hasOwnProperty('items') && videoInfo.items.length > 0;
  },
  /**
  * Get information by video id
  * @param {string} videoId
  * @return {json}
  */
  'Youtube.getVideoInfo'(videoId) {
    check(videoId, String);
    const videoInfo = Youtube.getVideoInfo(videoId);
    if (!videoInfo.hasOwnProperty('items') || videoInfo.items.length === 0) {
      return null;
    }
    return videoInfo.items[0];
  },
  /**
   * Get src information by video id
   * @param {string} videoId
   * @param {string} quality
   * @return {json}
   */
  'Youtube.getVideoSrc'(videoId, quality) {
    check(videoId, String);
    return Youtube.getVideoSrc(videoId, quality);
  },
  /**
  * Get information by video id
  * @param {string} videoId
  * @return {json}
  */
  'Youtube.getVideoSubtitles'(videoId) {
    check(videoId, String);
    return Youtube.getVideoSubtitles(videoId);
  },
  /**
   * Check exist video, status, privacy-status, license
   * @param {string} videoId
   * @return {boolean}
   */
  'Youtube.isAllowEmbedVideo'(videoId) {
    check(videoId, String);
    const videoInfo = Youtube.getVideoInfo(videoId);

    if (!videoInfo.hasOwnProperty('items') || videoInfo.items.length === 0) {
      return false;
    }
    const video = videoInfo.items[0];

    return video.status.embeddable
      && video.status.privacyStatus === 'public';
  },
});
