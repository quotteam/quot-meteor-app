import { $ } from 'meteor/jquery';
import alertify from 'alertify.js';


export class Notification {
  /**
   * Make a simple notification message with delay (15 sec)
   *
   * @param {string} message
   * @return {void}
   */
  static tipMessage (message) {
    alertify.delay(15000).success(message).logPosition("bottom left");
  }

  static errorMessage (message) {
    alertify.error(message).logPosition("bottom left");
  }

  static reset () {
    $('.alertify').remove();
    $('.alertify-logs').remove();
  }
}
