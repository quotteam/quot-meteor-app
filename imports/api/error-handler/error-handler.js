import { logger } from '/imports/api/logger/logger.js';

export class ErrorHandler {

  static get SYSTEM_ERROR () {
    return 1;
  }

  static get SYSTEM_CLIENT_ERROR () {
    return 2;
  }

  static get CLIENT_ERROR_NOTIFICATION_MSG () {
    return 'Возникла техническая неполадка. Попробуйте позже!';
  }


  /**
   *
   * @param {number} errorCode - constant from class ErrorHandler
   * @param {object} errorInfo - information object. e.g:
   * {
   *   loggerMessage: 'some message', // required field
   *   someField: <...>,
   *   ....
   * }
   * @param {function=} callback
   */
  static register (errorCode, errorInfo, callback) {
    const loggerMessage = errorInfo.loggerMessage;
    delete errorInfo.loggerMessage;
    logger.error(loggerMessage, errorInfo);
    if (callback) {
      callback();
    }
  }
}
