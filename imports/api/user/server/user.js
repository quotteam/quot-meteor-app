import {Meteor} from 'meteor/meteor';

Meteor.users.deny({
  update: function (userId, user) {
    if (user._id !== userId) {
      return false;
    }
  },
});
