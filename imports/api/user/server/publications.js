import { Meteor } from 'meteor/meteor';

Meteor.publish('User.data', function () {
  return this.userId ? Meteor.users.find({ _id: this.userId }, { fields: { 'profile': 1 } }) : null;
});