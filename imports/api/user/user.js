import {Meteor} from 'meteor/meteor';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';
import {UserCollectionItemSchema} from '../quot-collection/user/user-collection.js';

const UserSchema = {
  '_id': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  'createdAt': {
    type: Date,
  },
  'services': {
    type: Object,
    optional: true,
    blackbox: true
  },
  'profile': {
    type: Object,
    optional: true,
    blackbox: true
  },
  'quot_collection': {
    type: [UserCollectionItemSchema],
    optional: true
  }
};


Meteor.users.attachSchema(UserSchema);
