import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { HTTP } from 'meteor/http';
let Future = Npm.require('fibers/future');

Meteor.methods({
  /**
   * Create thumbnail for moment of youtube video

   * @param {string} videoId
   * @param {number|string} moment
   * @return {string|null}
   *
   * Moment may be a number (in seconds),
   * a percentage string (eg. "50%")
   * or a timestamp string with format "hh:mm:ss.xxx"
   * (where hours, minutes and milliseconds are both optional)
   */
  'Thumbnail.generate'(videoId, moment) {
    check(videoId, String);
    check(moment, Number);
    const future = new Future();
    HTTP.get(`${Meteor.settings.public.api.store.host}/ytimg/${videoId}/${moment}`,
      (error, result) => {
        if (result && result.statusCode === 200 && !result.data && result.data.hasOwnProperty('thumb')) {
          future.return(result.data.thumb);
        }
        else {
          future.return(null);
        }
      });
    return future.wait();
  },
});
