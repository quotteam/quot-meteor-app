import { $ } from 'meteor/jquery';

export class Slider {
  /**
   * @param {!Element} sliderElement
   * @param {number=} position
   */
  constructor(sliderElement, position) {
    /**@private {!Element} */
    this._slider = sliderElement;
    /**@private {number} **/
    this._position = position ? position : null;
    /**@private {!MeteorFlux.Dispatcher} */
    this._positionChangedEvent = new MeteorFlux.Dispatcher();
    /**@private {!MeteorFlux.Dispatcher} */
    this._stopedEvent = new MeteorFlux.Dispatcher();
    /**@private {!MeteorFlux.Dispatcher} */
    this._deactivateEvent = new MeteorFlux.Dispatcher();
    this._width = $(this._slider).find('.line').first().width();
  }

  /**
   * @return {!MeteorFlux.Dispatcher}
   */
  positionChangedEvent() {
    return this._positionChangedEvent;
  }

  /**
   * @return {!MeteorFlux.Dispatcher}
   */
  stopedEvent() {
    return this._stopedEvent;
  }

  /**
   * @param {number} positionInPercent
   */
  setPosition(positionInPercent) {
    if (!(0 <= positionInPercent && positionInPercent <= 100))
    {
      positionInPercent = this._position < positionInPercent ? 100 : 0;
    }
    if (this._position - positionInPercent)
    {
      if ($(this._slider).is('.velocity-animating'))
      {
        this.stop();
      }
      this._position = positionInPercent;
      this._slider.style.left = `${this._position}%`;
      this._positionChangedEvent.dispatch();
    }
  }

  /**
   * @param {number} positionInPercent
   * @param {number} durationInMilliseconds
   */
  runTo(positionInPercent, durationInMilliseconds) {
    if (!(0 <= positionInPercent && positionInPercent <= 100))
    {
      positionInPercent = this._position < positionInPercent ? 100 : 0;
    }
    if (this._position != positionInPercent)
    {
      const sliderEl = $(this._slider);
      sliderEl.velocity({left: `${positionInPercent}%`}, {
        duration: durationInMilliseconds,
        begin: undefined,
        progress: () => {
          this._position = parseInt(this._slider.style.left);
          this._positionChangedEvent.dispatch();
        },
        complete: () => {
          this._position = positionInPercent;
          this._stopedEvent.dispatch();
        },
        loop: false,
        delay: false
      });
    }
  }

  stop() {
    $(this._slider).velocity('stop');
    this._stopedEvent.dispatch();
  }

  /**
   * @return {number}
   */
  getPosition() {
    return this._position;
  }

  /**
   * @return {!Element}
   */
  getElement() {
    return this._slider;
  }

  /**
   * @return {!Number}
   */
  width() {
    return  this._width;
  }
}
