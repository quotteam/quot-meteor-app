import {Spinner} from './spinner.js';
import {Slider} from './slider.js';
import { $ } from 'meteor/jquery';

export class DistanceSpinnersController {
  /**
   * @param {!Element} timelineElement
   * @param {!Element} leftSpinnerElement
   * @param {!Element} rightSpinnerElement
   * @param {!Element} sliderElement
   */
  constructor(timelineElement, leftSpinnerElement, rightSpinnerElement, sliderElement) {
    this._timeline = timelineElement;
    this._leftSpinner = new Spinner(leftSpinnerElement);
    this._rightSpinner = new Spinner(rightSpinnerElement);
    this._slider = new Slider(sliderElement);
    this._interval = $(this._timeline).find('.interval')[0];
    this._intervalLineElement = $(this._interval).find('.line')[0];
    this._leftSpinnerDragEvent = new MeteorFlux.Dispatcher();
    this._rightSpinnerDragEvent = new MeteorFlux.Dispatcher();

    this._leftSpinner.positionChangedEvent().register(() => this._updateIntervalView());
    this._rightSpinner.positionChangedEvent().register(() => this._updateIntervalView());
  }

  /**
   * @return {!Spinner}
   */
  leftSpinner() {
    return this._leftSpinner;
  }

  /**
   * @return {!Spinner}
   */
  rightSpinner() {
    return this._rightSpinner;
  }

  /**
   * @return {!Slider}
   */
  slider() {
    return this._slider;
  }

  leftSpinnerDragEvent() {
    return this._leftSpinnerDragEvent;
  }

  rightSpinnerDragEvent() {
    return this._rightSpinnerDragEvent;
  }

  _onLeftSpinnerPress(event) {
    const mousemoveListener = (event) => {
      if (event.type == 'touchmove')
      {
        this._onLeftSpinnerDrag({
          x: event.touches[0].clientX,
          y: event.touches[0].clientY
        });
      }
      else
      {
        this._onLeftSpinnerDrag({
          x: event.clientX,
          y: event.clientY
        });
      }
    };
    event.stopPropagation();
    this._addMouseMoveListener(mousemoveListener);
    this._leftSpinner.getElement().addEventListener('touchmove', mousemoveListener);
    this._leftSpinner.getElement().addEventListener('touchend', () => {
      this._leftSpinner.getElement().removeEventListener('touchmove', mousemoveListener);
    });

    this._leftSpinner.activate();
    this._rightSpinner.deactivate();
  }

  _onRightSpinnerPress(event) {
    const mousemoveListener = (event) => {
      if (event.type == 'touchmove')
      {
        this._onRightSpinnerDrag({
          x: event.touches[0].clientX,
          y: event.touches[0].clientY
        });
      }
      else
      {
        this._onRightSpinnerDrag({
          x: event.clientX,
          y: event.clientY
        });
      }
    };
    event.stopPropagation();
    this._addMouseMoveListener(mousemoveListener);
    this._rightSpinner.getElement().addEventListener('touchmove', mousemoveListener);
    this._rightSpinner.getElement().addEventListener('touchend', () => {
      this._rightSpinner.getElement().removeEventListener('touchmove', mousemoveListener);
    });

    this._rightSpinner.activate();
    this._leftSpinner.deactivate();
  }

  /**
   * @param {{x: (number), y: (number)}=} mouseCoord
   */
  _onLeftSpinnerDrag(mouseCoord) {
    const containerCoord = this._timeline.getBoundingClientRect();
    const spinnerWidth = this._leftSpinner.getWidth();
    const newPosition = (mouseCoord.x - containerCoord.x + spinnerWidth / 2) * 100 / containerCoord.width;

    this._leftSpinner.setPosition(newPosition);
    if (newPosition >= this._rightSpinner.getPosition())
    {
      this._rightSpinner.setPosition(newPosition);
    }
    if (newPosition >= this._slider.getPosition())
    {
      this._slider.setPosition(newPosition);
    }

    this._leftSpinnerDragEvent.dispatch();
  }

  /**
   * @param {{x: (number), y: (number)}=} mouseCoord
   */
  _onRightSpinnerDrag(mouseCoord) {
    const containerCoord = this._timeline.getBoundingClientRect();
    const spinnerWidth = this._rightSpinner.getWidth();
    const newPosition = (mouseCoord.x - containerCoord.x - spinnerWidth / 2) * 100 / containerCoord.width;

    this._rightSpinner.setPosition(newPosition);
    if (newPosition <= this._leftSpinner.getPosition())
    {
      this._leftSpinner.setPosition(newPosition);
    }
    if (newPosition <= this._slider.getPosition())
    {
      this._slider.setPosition(newPosition);
    }

    this._rightSpinnerDragEvent.dispatch();
  }

  _updateIntervalView() {
    this._interval.style.left = this.leftSpinner().getElement().style.left;
    const leftSpinerRect = this.leftSpinner().getElement().getBoundingClientRect();
    const rightSpinnerRect = this.rightSpinner().getElement().getBoundingClientRect();
    this._intervalLineElement.style.width = `${rightSpinnerRect.left - leftSpinerRect.left}px`;
  }

  /**
   * @param {!Function} mousemoveListener
   * @private
   */
  _addMouseMoveListener(mousemoveListener) {
    window.addEventListener('mousemove', mousemoveListener);
    window.addEventListener('mouseup', () => {
      window.removeEventListener('mousemove', mousemoveListener);
    });
  }
}