import { $ } from 'meteor/jquery';
// import { MeteorFlux } from 'meteor/meteorflux:dispatcher';

const RIGHT_BUTTON_CODE = 39;
const LEFT_BUTTON_CODE = 37;

export class Spinner {
  /**
   * @param {!Element} spinnerElement
   * @param {number=} position
   */
  constructor(spinnerElement, position) {
    /**@private {!Element} */
    this._spinner = spinnerElement;
    /**@private {number} **/
    this._position = position ? position : null;
    /**@private {!MeteorFlux.Dispatcher} */
    this._positionChangedEvent = new MeteorFlux.Dispatcher();
    /**@private {!MeteorFlux.Dispatcher} */
    this._deactivateEvent = new MeteorFlux.Dispatcher();
    this._isActive = false;
  }

  /**
   * @return {!MeteorFlux.Dispatcher}
   */
  positionChangedEvent() {
    return this._positionChangedEvent;
  }

  /**
   * @param {number} positionInPercent
   */
  setPosition(positionInPercent) {
    if (!(0 <= positionInPercent && positionInPercent <= 100))
    {
      positionInPercent = this._position < positionInPercent ? 100 : 0;
    }
    if (this._position != positionInPercent)
    {
      this._position = positionInPercent;
      this._spinner.style.left = String(this._position + '%');
      this._positionChangedEvent.dispatch();
    }
  }

  /**
   * @return {boolean}
   */
  active() {
    return this._isActive;
  }

  /**
   * @return {number}
   */
  getPosition() {
    return this._position;
  }

  /**
   * @return {!Element}
   */
  getElement() {
    return this._spinner;
  }

  /**
   * @return {number}
   */
  getWidth() {
    return $(this._spinner).find('.ear').first().width();
  }

  /**
   * @param {string|number} label
   */
  setLabel(label) {
    $(this._spinner).find('.label')[0].textContent = label;
  }

  activate() {
    if (!this._isActive)
    {
      this._isActive = true;
      this._spinner.classList.add('active');

      const onKeyDown = (e) => {
        switch (e.keyCode) {
          case LEFT_BUTTON_CODE:
            this.setPosition(this.getPosition() - 0.01);
            break;
          case RIGHT_BUTTON_CODE:
            this.setPosition(this.getPosition() + 0.01);
            break;
        }
      };
      const onOutsideMouseDown = () => {
        this.deactivate();
      };

      window.addEventListener('mousedown', onOutsideMouseDown);
      window.addEventListener('keydown', onKeyDown);

      this._deactivateEvent.register(() => {
        window.removeEventListener('mousedown', onOutsideMouseDown);
        window.removeEventListener('keydown', onKeyDown);
      });

    }
  }

  deactivate() {
    if (this._isActive)
    {
      this._isActive = false;
      this._spinner.classList.remove('active');
      this._deactivateEvent.dispatch();
      this._deactivateEvent.reset();
    }
  }
}
