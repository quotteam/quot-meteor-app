import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';


class QuotSourceCollection extends Mongo.Collection {
  insert (doc, callback) {
    const ourDoc = doc;
    return super.insert(ourDoc, callback);
  }
}


export const QuotSource = new QuotSourceCollection('quot_sources');

QuotSource.schema = new SimpleSchema({
  '_id': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  'video_id': {
    type: String,
    optional: false,
    label: 'Youtube Id'
  },
  'authorChannelId': {
    type: String,
    label: 'Youtube author of source video',
    optional: true
  },
  'title': {
    type: String,
    label: 'Youtube video title',
    optional: true
  },
  'description': {
    type: String,
    label: 'Youtube source video description',
    optional: true
  },
  'tags': {
    type: [String],
    label: 'Youtube keywords for source video',
    optional: true
  },
  'transcriptions': {
    type: [String],
    label: 'Youtube transcription for source video',
    optional: true
  },
  'thumbnails': {
    type: Object,
    label: 'Youtube thumb object',
    blackbox: true
  },
});

QuotSource.attachSchema(QuotSource.schema);
export const QuotSourceSchema = QuotSource.schema;