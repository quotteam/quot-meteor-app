import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Quot, QuotSchema } from './quot.js';
import { QuotSource } from './quot-source.js';
import { ErrorHandler } from '../error-handler/error-handler';

/**
 * Create new quot with contain adding youtube source
 * @param {string} video_id
 * @param {object} interval {from: x, till: y}
 * @param {string} [title]
 * @param {string} [description]
 * @returns {string} [quotId]
 * @throws {Meteor.Error}
 */
export const InsertQuot = new ValidatedMethod({
  name: 'Quot.methods.insert',
  validate: QuotSchema.validator(),
  run ({ video_id, interval, title, description }) {

    const quot = {
      video_id: video_id,
      interval: interval,
      title: title,
      description: description,
      created_at: new Date(),
      thumbnail: `${video_id}-${interval.from}.jpg`, // TEMP SOLUTION
    };

    const quotId = Quot.insert(quot);

    // todo: refactoring request, IMPROVE IT
    Meteor.call('Thumbnail.generate', video_id, interval.from);

    const quotSource = QuotSource.findOne({ 'video_id': video_id });
    if (!quotSource) {
      Meteor.call('Youtube.getVideoInfo', video_id, (err, res) => {
        if (err) {
          ErrorHandler.register(ErrorHandler.SYSTEM_ERROR,
            { loggerMessage: 'Cannot get video information from youtube', videoId: video_id });
          throw new Meteor.Error('Quot.methods.insert',
            'Cannot get video information from youtube');
        }

        if (res && res.hasOwnProperty('snippet') && typeof res.snippet === 'object') {
          const videoData = res.snippet;
          const sourceData = {
            video_id: video_id,
            title: videoData.title,
            authorChannelId: videoData.channelId,
            tags: videoData.tags,
            transcriptions: [],
            description: videoData.description,
            thumbnails: videoData.thumbnails
          };

          const quotSourceId = QuotSource.insert(sourceData);

          Quot.update({ _id: quotId }, {
            '$set': {
              description: videoData.title,
              source_id: quotSourceId
            }
          });
        }
      });
    }
    else {
      Quot.update({ _id: quotId }, {
        '$set': {
          description: quotSource.title,
          source_id: quotSource._id
        }
      });
    }
    return quotId;
  },
});

/**
 * Add youtube source for existing quot
 * @param {string} quotId
 * @param {QuotSourceSchema} sourceData
 * @returns {number} updated items
 */
export const AddSourceToQuot = new ValidatedMethod({
  name: 'Quot.methods.addSource',
  validate: new SimpleSchema({
    quotId: { type: String },
    sourceId: { type: String }
  }).validator(),
  run ({ quotId, sourceId }) {
    // if quot not found - update return 0, no throw
    return Quot.update({ _id: quotId }, { '$set': { source_id: sourceId } });
  },
});

/**
 * Increment views for concrete quot
 * @param {string} quotId
 * @returns {void}
 */
export const IncrementView = new ValidatedMethod({
  name: 'Quot.methods.incrementView',
  validate: new SimpleSchema({
    quotId: { type: String },
  }).validator(),
  run ({ quotId }) {
    // TODO: make it simple
    const quot = Quot.findOne({ '_id': quotId });
    Quot.update({ _id: quotId }, { '$set': { 'view_count': quot.view_count + 1 } });
  },
});

// UNUSED
export const GenerateTranscriptionsToQuot = new ValidatedMethod({
  name: 'Quot.methods.generateTranscriptions',
  validate: new SimpleSchema({ quotId: { type: String } }).validator(),
  run ({ quotId }) {
    const quot = Quot.findOne({ _id: quotId });
    if (quot && typeof quot === 'object') {
      const videoId = quot.video_id;
      Meteor.call('Youtube.getVideoSubtitles', videoId, (err, res) => {
        if (err) {
          /* TODO: log it? */
        }
        if (res) {
          const transcriptions = res;
          Quot.update({ _id: quotId }, { '$set': { transcriptions: transcriptions } });
        }
      });
    }
  }
});

// // Get list of all method names on Todos
// const TODOS_METHODS = _.pluck([
//   insert,
//   setCheckedStatus,
//   updateText,
//   remove,
// ], 'name');
