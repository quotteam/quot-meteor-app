import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { QuotSource } from './quot-source';
import { ShortId } from 'meteor/mantarayar:shortid';

class QuotCollection extends Mongo.Collection {
  insert (doc, callback) {
    const ourDoc = doc;
    ourDoc._id = ShortId.generate();
    // ourDoc.createdAt = ourDoc.createdAt || new Date();
    return super.insert(ourDoc, callback);
  }

  transform (doc) {
    doc.source = QuotSource.findOne({
      _id: doc.source_id
    });
    return doc;
  }
}


export const Quot = new QuotCollection('quots');

Quot.deny({
  insert () {
    return true;
  },
  update () {
    return true;
  },
  remove () {
    return true;
  },
  transform () {
    return true;
  },
});

Quot.schema = new SimpleSchema({
  '_id': {
    type: String,
    //regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  'video_id': {
    type: String,
    optional: false,
    label: 'Youtube Id'
  },
  'title': {
    type: String,
    label: 'Quot title',
    optional: true
  },
  'description': {
    type: String,
    label: 'Quot description',
    optional: true
  },
  'thumbnail': {
    type: String,
    label: 'Generated thumbnail for quot moment',
    optional: true
  },
  'source_id': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    label: 'Youtube data source ref',
    optional: true
  },
  'interval': {
    type: Object,
    label: 'Quot time interval for playing'
  },
  'interval.from': { type: Number },
  'interval.till': { type: Number },
  'created_at': {
    type: Date,
    optional: true
  },
  'view_count': {
    type: Number,
    optional: true,
    defaultValue: 0
  }
});

Quot.attachSchema(Quot.schema);
export const QuotSchema = Quot.schema;
