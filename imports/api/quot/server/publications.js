import { Meteor } from 'meteor/meteor'; 
import { Quot } from '../quot.js';
import { QuotSource } from '../quot-source.js';
import { check } from 'meteor/check';

Meteor.publish('Quot.getOne', (quotId) => {
  check(quotId, String);
  return Quot.find({'_id': quotId });
});

Meteor.publish('QuotSource.getOne', (sourceId) => {
  check(sourceId, String);
  return QuotSource.find({ '_id': sourceId });
});
