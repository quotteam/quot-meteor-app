import { Mongo } from 'meteor/mongo';
import { Fixtures } from 'meteor/moonco:fixtures';
import { InsertQuot } from '../methods.js';

export const QuotFixture = () => {
  InsertQuot.call({
    video_id: 'f8SLpDBsurk',
    interval: { from: 30, till: 45 },
    title: 'some title',
    description: 'some description'
  }, (err, res) => {
    if (err) {
      console.log(err);
    }
  });

  InsertQuot.call({
    video_id: '4v5duJIN5Iw',
    interval: { from: 10, till: 15 },
    title: 'without description'
  }, (err, res) => {
    if (err) {
      console.log(err);
    }
  });

  InsertQuot.call({
    video_id: 'M9HZOt0BIt0',
    interval: { from: 10, till: 15 },
    title: 'without description'
  }, (err, res) => {
    if (err) {
      console.log(err);
    }
  });

  /* Example for fixtures via mongo db insertions
  Fixtures.create(Quot, (api) => {
    api.flush();

    api.insert('testing_quot_1',
      {
        '_id': 'H5HY2iezGzr6d6x45',
        'video_id': 'S9wTatnT21s',
        'interval': {
          'from': 10,
          'till': 20
        },
        'created_at': '2017-11-29T14:35:24.573+0000',
        'view_count': 0
      });

    api.insert('testing_quot_2',
      {
        '_id': 'S9wTatnT2zr6d6x45',
        'video_id': 'K3cwlckcALU',
        'interval': {
          'from': 10,
          'till': 30
        },
        'created_at': '2017-11-29T14:35:24.573+0000',
        'view_count': 0
      });

    api.insert('testing_quot_3',
      {
        '_id': 'S9wTatnT2zr6d6x46',
        'video_id': 'K3cwlckcALU',
        'interval': {
          'from': 10,
          'till': 12
        },
        'created_at': '2017-11-29T14:35:24.573+0000',
        'view_count': 0,
        'source': {
          'description': 'KAKИЕ-ТО БАРЦУХЕ'
        }
      });
    });
  */
};
