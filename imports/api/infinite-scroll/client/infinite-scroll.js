
import { $ } from 'meteor/jquery';
import { Session } from 'meteor/session';

export class InfiniteScroll {
  /**
   * @param {String} targetSelector
   * @param {String} sessionLimitName
   * @param {Number} limit
  */
  constructor(targetSelector, sessionLimitName, limit) {
    this.targetSelector = targetSelector;
    this.sessionLimitName = sessionLimitName;
    this.limit = limit;
    Session.setDefault(this.sessionLimitName, this.limit);
    $(window).scroll(() => {
      const target = $(this.targetSelector);
      if (!target.length) { 
        return;
      }
      const threshold = $(window).scrollTop() + $(window).height() - target.height();
      if (target.offset().top < threshold) {
        if (!target.data('visible')) {
          target.data('visible', true);
          Session.set(this.sessionLimitName, Session.get(this.sessionLimitName) + this.limit);
        }
      } else {
        if (target.data('visible')) {
          target.data('visible', false);
        }
      }     
    });
  }
}
