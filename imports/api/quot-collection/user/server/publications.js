import { Meteor } from 'meteor/meteor';
import { Quot } from '/imports/api/quot/quot.js';
import { QuotSource } from '/imports/api/quot/quot-source.js';
import { check, Match } from 'meteor/check';
import { publishComposite } from 'meteor/reywood:publish-composite';
import { ErrorHandler } from '/imports/api/error-handler/error-handler.js';


publishComposite('QuotCollection.user.search', (searchText, limit) => {

  check(searchText, Match.OneOf(String, null));
  check(limit, Number);

  return {
    find: function () {
      const user = Meteor.users.findOne({ _id: Meteor.userId() });
      if (!user) {
        ErrorHandler.register(ErrorHandler.SYSTEM_ERROR,
          { loggerMessage: 'Search in user collection fail - user not found', userId: Meteor.userId() });
        throw new Meteor.Error('QuotCollection.user.search',
          'User not auth for publication');
      }
      const userCollection = user && user.hasOwnProperty('quot_collection') && Array.isArray(user.quot_collection) ?
                             user.quot_collection : [];
      let userCollectionQuotIds = [];
      let userCollectionQuotIdAsAddedAt = [];
      if (userCollection.length > 0) {
        userCollectionQuotIds = userCollection.map((collectionItem) => {
          userCollectionQuotIdAsAddedAt[collectionItem.quotId] = collectionItem.addedAt;
          return collectionItem.quotId;
        });
      }

      if (searchText) {
        return Quot.find(
          {
            _id: { $in: userCollectionQuotIds },
            $or: [{ title: { $regex: searchText, $options: 'i' } },
              { description: { $regex: searchText, $options: 'i' } }]
          },
          {
            score: { $meta: "textScore" },
            transform: function (quot) {
              quot.addedAt = userCollectionQuotIdAsAddedAt[quot._id];
              return quot;
            }
          });
      }
      else {
        return Quot.find({ _id: { $in: userCollectionQuotIds } }, {
          limit: limit,
          transform: function (quot) {
            quot.addedAt = userCollectionQuotIdAsAddedAt[quot._id];
            return quot;
          },
        });
      }
    },
    children: [
      {
        find (quot) {
          return QuotSource.find({ _id: quot.source_id });
        }
      }
    ]
  };
});
