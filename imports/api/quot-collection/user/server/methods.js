import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ErrorHandler } from '/imports/api/error-handler/error-handler';
import { UserCollectionItemSchema } from '../user-collection.js';


/**
 * Add quot by quotId to user collection
 * (!) user auth required
 * for call from client use: Meteor.call('Quots.UserCollection.add', ({quotId: '...'}))
 *
 *
 * @param {string} quotId
 * @returns {bool}
 */
export const AddQuotToUserCollection = new ValidatedMethod({
  name: 'Quots.UserCollection.add',
  validate: new SimpleSchema(UserCollectionItemSchema).validator(),
  run ({ quotId }) {
    if (!Meteor.userId()) {
      ErrorHandler.register(ErrorHandler.SYSTEM_ERROR,
        { loggerMessage: 'Attempt to add quot in collection without authorization', quotId: quotId });
      return false;
      //throw new Meteor.Error('quot.collection.user.methods.add',
      //  'User must be auth');
    }
    // this return promise!!
    Meteor.users.findAndModify({
      query: { _id: Meteor.userId() },
      update: { $push: { quot_collection: { quotId: quotId, addedAt: new Date() } } }
    });

    return true;
  },
});

/**
 * Remove quot from user collection
 * (!) user auth required
 * for call from client use: Meteor.call('Quots.UserCollection.remove', ({quotId: '...'}))
 *
 *
 * @param {string} quotId
 * @returns {bool}
 */
export const RemoveQuotFromUserCollection = new ValidatedMethod({
  name: 'Quots.UserCollection.remove',
  validate: new SimpleSchema(UserCollectionItemSchema).validator(),
  run ({ quotId }) {
    if (!Meteor.userId()) {
      ErrorHandler.register(ErrorHandler.SYSTEM_ERROR,
        { loggerMessage: 'Attempt to remove from collection quot without authorization', quotId: quotId });
      return false;
    }
    // this return promise!!
    Meteor.users.update({ _id: Meteor.userId() },
      { $pull: { quot_collection: { quotId: quotId } } });

    return true;
  },
});

export const IsQuotInUserCollection = new ValidatedMethod({
  name: 'Quots.UserCollection.isExistQuot',
  validate: new SimpleSchema(UserCollectionItemSchema).validator(),
  run ({ quotId }) {
    if (!Meteor.userId()) {
      ErrorHandler.register(ErrorHandler.SYSTEM_ERROR,
        { loggerMessage: 'Attempt to get from collection quot without authorization', quotId: quotId });
      return false;
    }
    //const findQuot = Meteor.users.find({ quot_collection: { $in: [quotId] } }); // if quot stored in array
    const findQuot = Meteor.users.find({
      _id: Meteor.userId(),
      quot_collection: {
        $exists: true,
        $elemMatch: { quotId: quotId }
      }
    });
    return findQuot.count() > 0;
  },
});
