import { SimpleSchema } from 'meteor/aldeed:simple-schema';


export const UserCollectionItemSchema = {
  'quotId': {
    type: String
    //regEx: SimpleSchema.RegEx.Id,
  },
  'addedAt': {
    type: Date,
    optional: true
  }
};
