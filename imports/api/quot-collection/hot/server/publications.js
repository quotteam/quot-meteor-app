import { Meteor } from 'meteor/meteor';
import { Quot } from '/imports/api/quot/quot.js';
import { QuotSource } from '/imports/api/quot/quot-source.js';
import { check, Match } from 'meteor/check';
import { publishComposite } from 'meteor/reywood:publish-composite';

publishComposite('QuotCollection.hot.search', (searchText, limit) => {

  check(searchText, Match.OneOf(String, null));
  check(limit, Number);

  return {
    find: function () {

      if (searchText) {
        return Quot.find(
          {
            $or: [{ title: { $regex: searchText, $options: 'i' } },
              { description: { $regex: searchText, $options: 'i' } }]
          },
          { fields: { score: { $meta: 'textScore' } }, sort: { score: { $meta: 'textScore' } } });
      }
      else {
        return Quot.find({},
          {
            limit: limit,
            sort: { view_count: -1 }
          });
      }
    },
    children: [
      {
        find (quot) {
          return QuotSource.find({ _id: quot.source_id });
        }
      }
    ]
  };
});


