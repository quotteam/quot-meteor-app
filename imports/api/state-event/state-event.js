import { Session } from 'meteor/session';
import { Meteor } from "meteor/meteor";
import { logger } from '/imports/api/logger/logger.js';


export class StateEvent {
  /**
   * Register event for system
   *
   * @param {string} eventName
   * @param {string} storageType
   * @return {void}
   */
  static register (eventName, storageType) {
    if (Session.get(eventName)) {
      return;
    }
    if (storageType === 'persistent') {
      Session.setPersistent(eventName, 'registered');
    }
    else if (storageType === 'temporary') {
      Session.setTemp(eventName, 'registered');
    }
    else {
      logger.info('StateEvent unknow parameter', { eventName: eventName, storageType: storageType });
      throw new Meteor.Error('StateEvent',
        'Unknow storage type parameter');
    }
  }

  /**
   * Run registered event by name
   *
   * @param {string} eventName
   * @param {function} func
   * @return {void}
   */
  static run (eventName, func) {
    const event = Session.get(eventName);
    if (event && event === 'registered') {
      Session.update(eventName, 'done');
      func();
    }
  }

  /**
   * Unregister event
   *
   * @param {string} eventName
   * @return {void}
   */
  static unregister (eventName) {
    Session.clear(eventName);
  }
}
