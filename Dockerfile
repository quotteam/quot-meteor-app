FROM ubuntu:16.04
RUN apt-get update -y && \
    apt-get install -yqq --no-install-recommends apt-transport-https ca-certificates curl nodejs-legacy && \
    apt-get clean && apt-get autoclean && apt-get autoremove && \
    curl https://install.meteor.com/
